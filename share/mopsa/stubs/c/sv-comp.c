/*$
 * ensures: raise("reach_error called");
 * ensures: 1 == 0;
 */
void reach_error (void);
